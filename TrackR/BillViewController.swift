//
//  BillViewController.swift
//  TrackR
//
//  Created by Student on 4/17/18.
//  Copyright © 2018 Targaryeans. All rights reserved.
//

import UIKit

class BillViewController: UIViewController {

    var data:Data?
    
    @IBOutlet weak var bill: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
//        print(data!)
        self.bill.image=UIImage(data: data!)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
