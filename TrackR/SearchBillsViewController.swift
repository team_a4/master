//
//  SearchBillsViewController.swift
//  TrackR
//
//  Created by Student on 4/17/18.
//  Copyright © 2018 Targaryeans. All rights reserved.
//

import UIKit
import CoreData

class SearchBillsViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    var details:[String:[[String:Any]]]=[:]
    var dates:[[String:Any]]=[[:]]
    let datePicker = UIDatePicker()
    var stores:[String] = []
    var selectedStore:String? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        self.showDatePicker()
/*        if let detail=UserDefaults.standard.object(forKey: "details") as? [String:[[String:Any]]]{
            self.details=detail
        }*/
        stores = DBUtil.sharedInstance.getStoreNames()
        let pickerView = UIPickerView()
        pickerView.delegate = self
        store.inputView = pickerView
        
        let toolbar = self.getToolbar(target: store, done: #selector(resignFirstResponder), cancel: #selector(resignFirstResponder))
        store.inputAccessoryView = toolbar
        // Do any additional setup after loading the view.
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var store: UITextField!
    @IBOutlet weak var date: UITextField!
    @IBAction func search(_ sender: Any) {
        let storeName=self.store.text?.uppercased()
//        print(storeName!)
        if storeName! != ""{
            if let details = DBUtil.sharedInstance.getData(forKey: "store", value: storeName!) {
                self.dates=details as! [[String : Any]]
                if date.text != "" {
                    var array:[[String: Any]] = []
                    for item in details {
                        let row = item as! NSDictionary
                        if date.text == row["date"] as? String {
                            array.append(row as! [String : Any])
                        }
                    }
                    self.dates=array
                    self.performSegue(withIdentifier: "toDates", sender: array)
                    return
                }
                self.performSegue(withIdentifier: "toDates", sender: details)
            }
            
        }
        else if date.text != "" {
            if let details = DBUtil.sharedInstance.getData(forKey: "date", value: date.text!) {
                self.dates=details as! [[String : Any]]
                self.performSegue(withIdentifier: "toDates", sender: self.dates)
            }
        }
        else{
            self.alertBox(title: "Please select store name", message: "")
        }

    }
    func alertBox(title:String, message:String){
        let alert=UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertAction=UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (alertAction) in
            
        }
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let a=segue.destination as? BillsDateTableViewController{
                a.b=self.dates
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Input Acessories
    
    func getToolbar(target:Any, done:Selector, cancel:Selector) -> UIToolbar {
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: done);
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: target, action: cancel);
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        return toolbar
    }
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = self.getToolbar(target: self, done: #selector(donedatePicker), cancel: #selector(cancelDatePicker))
        
        date.inputAccessoryView = toolbar
        date.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        date.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }

    //MARK: - Store Picker Delegates
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return stores.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return stores[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if stores.count > 0 {
            self.selectedStore = stores[row]
            store.text = stores[row]
        }
    }
}
