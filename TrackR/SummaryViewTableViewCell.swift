//
//  SummaryViewTableViewCell.swift
//  TrackR
//
//  Created by Jaladanki,Sai Praneetha on 2/16/18.
//  Copyright © 2018 Targaryeans. All rights reserved.
//

import UIKit

class SummaryViewTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBOutlet weak var headerLBL: UILabel!
    @IBOutlet weak var amountLBL: UILabel!
    
}
