//
//  SearchViewController.swift
//  TrackR
//
//  Created by Alapati,Lakshmi Manjari on 2/16/18.
//  Copyright © 2018 Targaryeans. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{
    
    @IBOutlet weak var FindByDateOrMonthLBL: UILabel!
    @IBOutlet weak var selectDataLBL: UILabel!
    @IBOutlet weak var storePickerLBL: UILabel!
    @IBOutlet weak var monthPickerLBL: UILabel!
    @IBOutlet weak var enterAmountLBL: UILabel!
    @IBOutlet weak var dollarOneLBL: UILabel!
    @IBOutlet weak var dollarTwoLBL: UILabel!
    @IBOutlet weak var MinTF: UITextField!
    @IBOutlet weak var MaxTF: UITextField!
    @IBOutlet weak var StorePicker: UIPickerView!
    @IBOutlet weak var FindByDateOrMonthPicker: UIPickerView!
    @IBOutlet weak var MonthPicker: UIPickerView!
    @IBOutlet weak var DatePicker: UIDatePicker!
    
    @IBAction func AddFilterAction(_ sender: UIButton) {
        FindByDateOrMonthLBL.isHidden = false
        selectDataLBL.isHidden = false
        monthPickerLBL.isHidden = false
        enterAmountLBL.isHidden = false
        FindByDateOrMonthPicker.isHidden = false
        DatePicker.isHidden = false
        MonthPicker.isHidden = false
        dollarOneLBL.isHidden = false
        dollarTwoLBL.isHidden = false
        MinTF.isHidden = false
        MaxTF.isHidden = false
    }
    
    @IBAction func ClearFilterAction(_ sender: UIButton) {
        FindByDateOrMonthLBL.isHidden = true
        selectDataLBL.isHidden = true
        monthPickerLBL.isHidden = true
        enterAmountLBL.isHidden = true
        FindByDateOrMonthPicker.isHidden = true
        DatePicker.isHidden = true
        MonthPicker.isHidden = true
        dollarOneLBL.isHidden = true
        dollarTwoLBL.isHidden = true
        MinTF.isHidden = true
        MaxTF.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        StorePicker.delegate = self
        StorePicker.dataSource = self
        FindByDateOrMonthPicker.delegate = self
        FindByDateOrMonthPicker.dataSource = self
        MonthPicker.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    var stores:[String] = ["All", "Walmart", "Hy-vee", "Walgreens"]
    var options:[String] = ["-Select-", "Date", "Month"]
    var months:[String] = ["January","Febraury","March","April","May","June","July","August","September","October","November","December"]
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == StorePicker) {
            return stores.count
        }
        else if pickerView == FindByDateOrMonthPicker{
            return options.count
        }
        else if pickerView == MonthPicker{
            return months.count
        }
        return 3
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == StorePicker) {
            return stores[row]
        }
        else if pickerView == FindByDateOrMonthPicker{
            return options[row]
        }
        else if pickerView == MonthPicker{
            return months[row]
        }
        return nil
    }
    
    @IBAction func resetToOriginalState(sender: UIButton) {
//        self.resetToOriginalState (sender: UIButton)
          self.resetToOriginalState (sender: sender)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
