//
//  SummaryViewController.swift
//  TrackR
//
//  Created by Alapati,Lakshmi Manjari on 2/16/18.
//  Copyright © 2018 Targaryeans. All rights reserved.
//

import UIKit

class SummaryViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDataSource, UITableViewDelegate{

    var details:[String:[[String:Any]]]=[:]
    var dates:[[String:Any]]=[[:]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        StorePicker.delegate = self
//        StorePicker.dataSource = self
        self.title = "Summary"
        self.tableView.register(SummaryViewTableViewCell.self, forCellReuseIdentifier: "data")
        self.showDatePicker()
        stores.append(contentsOf: DBUtil.sharedInstance.getStoreNames())
        let pickerView = UIPickerView()
        pickerView.delegate = self
        store.inputView = pickerView
/*        if let details=UserDefaults.standard.object(forKey: "details") as? [String:[[String:Any]]] {
            self.details = details
            //print(details)
            let arr=Array(details.keys)
            stores.append(contentsOf: arr)
            let pickerView = UIPickerView()
            pickerView.delegate = self
            store.inputView = pickerView
        }*/
        // Do any additional setup after loading the view.
        
        let toolbar = self.getToolbar(target: store, done: #selector(resignFirstResponder), cancel: #selector(resignFirstResponder))
        store.inputAccessoryView = toolbar
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    let datePicker = UIDatePicker()
    @IBOutlet weak var store: UITextField!
    @IBOutlet weak var date: UITextField!
    @IBOutlet weak var StorePicker: UIPickerView!
    @IBOutlet weak var tableView: UITableView!
    
    var stores:[String] = ["All"]
    var selectedStore:String? = nil
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return stores.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return stores[row]
    }
  
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedStore = "All"
        if row>0 {
            self.selectedStore = stores[row-1]
        }
        store.text = stores[row]
    }
    var headers:[String] = ["Monthly: ","yearly: ","Till date: " ]
    var amounts:[Double] = [0.0, 0.0, 0.0]
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return headers.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "data", for: indexPath) as! SummaryViewTableViewCell
//        cell.headerLBL.text = headers[indexPath.row]
//        cell.amountLBL.text = "200"
        let cell = UITableViewCell(style: .value1, reuseIdentifier: nil)
        cell.textLabel?.text = headers[indexPath.row]
        cell.detailTextLabel?.text = String(amounts[indexPath.row])
        return (cell)
    }
    
    func calculateOverAll(array:[[String: Any]]) -> Double {
        var total = 0.0
        for item in array {
            if let amt = item["amount"] {
                total += (amt as! Double)
            }
        }
        return total
    }
    
    func calculateForYear(array:[[String: Any]], year:String) -> Double {
        var total = 0.0
        for item in array {
            let date = item["date"] as! String
            if date.split(separator: "/")[2] == year {
                if let amt = item["amount"] {
                    total += (amt as! Double)
                }
            }
        }
        return total
    }
    
    func calculateForMonth(array:[[String: Any]],month:String, year:String) -> Double {
        var total = 0.0
        for item in array {
            let date = item["date"] as! String
            if date.split(separator: "/")[2] == year && date.split(separator: "/")[0] == month {
                if let amt = item["amount"] {
                    total += (amt as! Double)
                }
            }
        }
        return total
    }
    
    @IBAction func search(_ sender: Any) {
        amounts = [0.0, 0.0, 0.0]
        let storeName=self.store.text?.uppercased()
        //print(Array(self.details.values))
        if storeName! != ""{
            if storeName == "ALL"{
                if let data = DBUtil.sharedInstance.getData(forKey: nil, value: nil) {
                    let allArr:[[String: Any]] = data as! [[String: Any]]
                    amounts[2] = self.calculateOverAll(array: allArr)
                    if date.text != "" {
                        if let components = date.text?.split(separator: "/") {
                            amounts[1] = self.calculateForYear(array: allArr, year: String(components[2]))
                            amounts[0] = self.calculateForMonth(array: allArr, month: String(components[0]), year: String(components[2]))
                        }
                    }
                }
            }
            else {
                
                if let data = DBUtil.sharedInstance.getData(forKey: "store", value: storeName) {
                    let allArr:[[String: Any]] = data as! [[String: Any]]
                    amounts[2] = self.calculateOverAll(array: allArr)
                    if date.text != "" {
                        if let components = date.text?.split(separator: "/") {
                            amounts[1] = self.calculateForYear(array: allArr, year: String(components[2]))
                            amounts[0] = self.calculateForMonth(array: allArr, month: String(components[0]), year: String(components[2]))
                        }
                    }
                }
            }
            
                //                self.alertBox(title: "Store", message: "not found")
        }else{
            self.alertBox(title: "Please enter store name", message: "")
        }
        self.tableView.reloadData()
    }
    func alertBox(title:String, message:String){
        let alert=UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertAction=UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (alertAction) in
            
        }
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func getToolbar(target:Any, done:Selector, cancel:Selector) -> UIToolbar {
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: done);
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: target, action: cancel);
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        return toolbar
    }
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = self.getToolbar(target: self, done: #selector(donedatePicker), cancel: #selector(cancelDatePicker))
        date.inputAccessoryView = toolbar
        date.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        date.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }

}


/*
["WALMART": [["04/19/2018": , "amount": 10], ["04/18/2018": , "amount": 20], ["03/19/2018": , "amount": 15], ["03/16/2018": , "amount": 25]], "A&B": [["04/16/2018": , "amount": 60], ["03/17/2018": , "amount": 65]]]
 */

