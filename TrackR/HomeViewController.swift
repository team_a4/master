//
//  HomeViewController.swift
//  TrackR
//
//  Created by Student on 2/14/18.
//  Copyright © 2018 Targaryeans. All rights reserved.
//

import UIKit
//import SQLite
import CoreData

class HomeViewController: UIViewController {
    
//    var database: Connection!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton=true
//        do {
//            let documentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
//            let fileUrl = documentDirectory.appendingPathComponent("users").appendingPathExtension("sqlite3")
//            print(fileUrl)
//            let database = try Connection(fileUrl.path)
//            self.database = database
//            print("connected")
//        } catch {
//            print(error)
//        }
        // Do any additional setup after loading the view, typically from a nib.
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Expenses")
        //request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                print(data)
                //print(data.value(forKey: "username") as! String)
            }
            
        } catch {
            
            print("Failed")
        }
    }
    @IBAction func upload(_ sender: Any) {
        self.performSegue(withIdentifier: "upload", sender: nil)
    }
    
    @IBAction func summary(_ sender: Any) {
    }
    @IBAction func search(_ sender: Any) {
        self.performSegue(withIdentifier: "search", sender: nil)
    }
    @IBOutlet weak var search: UIButton!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func unwind(for unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {
        
    }

}

