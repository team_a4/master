# README #
*******************************************************************

Team A4: The Targareyans!
TrackR - Receipt Tracker

*******************************************************************
Prpject Repository: https://bitbucket.org/team_a4/master
Download the App and deploy in it on your device for camera functionality.

For a succesful deployment, we need to have the version of the app equal or lower than the version Xcode on the Mac.

No credentials are needed to run this app.

Instructions to use the app:

1. Open the app in the device.
2. A homescreen of the app is observed. It has three buttons namely Upload Receipt, Search receipt and Summary. Each button has its respective functionality.
3. To upload a receipt, click on the Upload receipt button.
4. Click on the upload button to take a picture of the receipt.
5. Manually enter the store name, pick the date using a date picker and enter the amount of the receipt.
6. Click on the save photo button. The image of the receipt is stored.
7. You will be redirected to the homescreen.
8. Click on the Search receipt button. Enter both the store name and store date using the pickers or either of the fields provided to search the receipt you want to.
9. Click on the search button.
10. A list of receipts will appear date wise. Click on any date to obtain the image of the receipt.
11. Click on back to return to the homescreen.
12. To use the summary functionality, click on the Summary button.
13. Enter both the store name and the date using pickers or either of the fields and click on search.
14. Observe the results in the middle section of the screen where you can observe the monthly, yearly and the total expense till date.
15. Choose 'All' in the store name picker to see the total amount of all stores.
16. Exit and navigate to the homescreen using the Back button.

Feature		                                        Status
Uploading a bill by clicking an image of the bill	Completed and working
Saving the bills	                                Completed and working
Tracking the monthly expenses	                        Completed and working
Searching the bills by store name	                Completed and working
Searching the bills by date	                        Completed and working
Database Connectivity	                                Completed and working
To run on the app on the Mac through a simulator, you need to change the property to .photolibrary instead of .camera in the UploadViewController.

