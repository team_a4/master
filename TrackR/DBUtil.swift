//
//  DBUtil.swift
//  TrackR
//
//  Created by Student on 4/22/18.
//  Copyright © 2018 Targaryeans. All rights reserved.
//

import Foundation
import UIKit
import CoreData

final class DBUtil {
    static let sharedInstance = DBUtil()
    
    func getStoreNames() -> [String]{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Expenses")
        request.resultType = .dictionaryResultType
        request.propertiesToFetch = ["store"]
        request.returnsObjectsAsFaults = false
        request.returnsDistinctResults = true
        var stores:[String] = []
        do {
            let result = try context.fetch(request)
            for str in result {
                let v = str as! NSDictionary
                stores.append(v["store"] as! String)
            }
        } catch {
            print("Failed")
        }
        return stores
    }
    
    func getData(forKey key:String?, value:String?) -> [Any]? {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Expenses")
        if let attr = key, let val = value {
            request.predicate = NSPredicate(format: "%K = %@", attr, val)
        }
        request.resultType = .dictionaryResultType
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            return result
            //            for data in result as! [NSManagedObject] {
            //                print(data)
            //                //print(data.value(forKey: "username") as! String)
            //            }
            
        } catch {
            
            print("Failed")
        }
        return nil
    }
}
