//
//  ScannerViewController.swift
//  TrackR
//
//  Created by Student on 4/2/18.
//  Copyright © 2018 Targaryeans. All rights reserved.
//

import UIKit

import AVFoundation

class ScannerViewController: UIViewController,AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet var square: UIImageView!
    var video=AVCaptureVideoPreviewLayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //creating a session
        let session=AVCaptureSession()
        
        //Define capturing device
        let captureDevice=AVCaptureDevice.default(for:AVMediaType.video)
        do{
            let input=try AVCaptureDeviceInput(device: captureDevice!)
            session.addInput(input)
        }
        catch{
            print("error")
        }
        
        let output=AVCaptureMetadataOutput()
        session.addOutput(output)
        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        output.metadataObjectTypes=[AVMetadataObject.ObjectType.qr]
        video=AVCaptureVideoPreviewLayer(session: session)
        video.frame=view.layer.bounds
        view.layer.addSublayer(video)
        self.view.bringSubview(toFront: square)
        session.startRunning()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects != nil && metadataObjects.count != 0{
            
            print("kanni \(metadataObjects)")
            if let object=metadataObjects[0] as? AVMetadataMachineReadableCodeObject{
                
                
                if object.type==AVMetadataObject.ObjectType.qr{
                    let alert=UIAlertController(title: "OR Code", message: object.stringValue, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Retake", style: UIAlertActionStyle.default, handler: nil))
                    alert.addAction(UIAlertAction(title: "Copy", style: UIAlertActionStyle.default, handler: { (nil) in
                        UIPasteboard.general.string=object.stringValue
                        let url=(object.stringValue)!
                        print(url)
                        //                        let urla=URL(string:url)!
                        ////                        let request=URLRequest(url: url)
                        //
                        //                        let a=URLSession.shared.dataTask(with: urla, completionHandler: { (data, response, error) in
                        //                            if error != nil{
                        //                                print("error")
                        //                            }else{
                        //                                print("data \(data)")
                        //
                        //                                if let unwrapperdData=data{
                        //                                    let dataString=NSString(data:unwrapperdData,encoding:String.Encoding.utf8.rawValue)
                        //                                    print(dataString)
                        //
                        //                                    let separator="<div class=\"text\">"
                        //                                    let b=dataString?.components(separatedBy: separator)
                        //                                    let c=b![1].components(separatedBy: "</div>")
                        //                                    let d=c[0]
                        //                                    print("array \(d.components(separatedBy: "\n"))")
                        //                                }
                        //                            }
                        //
                        //                        })
                        //                        a.resume()
                        //
                        
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
}
