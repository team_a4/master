//
//  UploadViewController.swift
//  TrackR
//
//  Created by Student on 2/14/18.
//  Copyright © 2018 Targaryeans. All rights reserved.
//

import UIKit
import CoreData

class UploadViewController: UIViewController, UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    
    var details:[String:[[String:Any]]]=[:]
    let datePicker = UIDatePicker()
    @IBOutlet weak var store: UITextField!
    @IBOutlet weak var date: UITextField!
    @IBOutlet weak var amount: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showDatePicker()
        // Do any additional setup after loading the view, typically from a nib.
        let toolbar = self.getToolbar(target: store, done: #selector(resignFirstResponder), cancel: #selector(resignFirstResponder))
        store.inputAccessoryView = toolbar

        let toolbar1 = self.getToolbar(target: amount, done: #selector(resignFirstResponder), cancel: #selector(resignFirstResponder))
        amount.inputAccessoryView = toolbar1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBOutlet weak var imageView: UIImageView!
    
    @IBAction func upload(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageView.contentMode = .scaleToFill
            imageView.image = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    @IBAction func Save(_ sender: Any) {
        
        if self.date.text != "" && self.store.text != "" && self.amount.text != ""{
            let imageData = UIImagePNGRepresentation(imageView.image!)
            let s=((self.store.text?.uppercased())!)
            let d=(self.date.text!)
            let a = self.amount.text!
/*            if let detail=UserDefaults.standard.object(forKey: "details") as? [String:[[String:Any]]]{
                self.details=detail
                let arr=Array(self.details.keys)
                if(arr.contains(s)){
                    self.details[s]?.append([d:imageData!, "amount":a])
                }else{
                     self.details[s]=[[d:imageData!, "amount":a]]
                }
                
                
            }else{
                self.details=[s:[[d:imageData!, "amount":a]]]
            }
            UserDefaults.standard.set(self.details, forKey: "details")
//            print("kanni \(self.details)  kanni")
*/
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            let entity = NSEntityDescription.entity(forEntityName: "Expenses", in: context)
            let newEntry = NSManagedObject(entity: entity!, insertInto: context)
            newEntry.setValue(s, forKey: "store")
            newEntry.setValue(d, forKey: "date")
            newEntry.setValue(Double(a), forKey: "amount")
            newEntry.setValue(imageData, forKey: "image")
            do {
                try context.save()
            } catch {
                print("Failed saving")
            }
            
            self.performSegue(withIdentifier: "toHome", sender: nil)
        }else{
            self.alertBox(title: "Fields", message: "cant be empty")
        }
        
        
        
    }
    func alertBox(title:String, message:String){
        let alert=UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertAction=UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (alertAction) in
            
        }
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
    }
    func getToolbar(target:Any, done:Selector, cancel:Selector) -> UIToolbar {
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: done);
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: target, action: cancel);
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        return toolbar
    }
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = self.getToolbar(target: self, done: #selector(donedatePicker), cancel: #selector(cancelDatePicker))
        
        date.inputAccessoryView = toolbar
        date.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        date.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}

